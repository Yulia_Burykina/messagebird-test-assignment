<?php

require_once 'vendor/autoload.php';

$app = \App\MessageApplicationFactory::createHttpApplication();

$request = [
    'content' => file_get_contents('php://input'),
    'method' => $_SERVER['REQUEST_METHOD']
];

$response = $app->execute($request);

$response->send();