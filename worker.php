<?php

require_once 'vendor/autoload.php';

$submitter = \App\MessageApplicationFactory::createWorker();

$submitter->run();
