<?php

namespace App;

use Config\Config;
use Services\AMQPClient;
use Services\MessageCreator;
use Services\MessageValidator;
use MessageBird\Client;

class MessageApplicationFactory
{

    public static function createHttpApplication(): HttpApplication
    {
        $validator = new MessageValidator();
        $sender = new AMQPClient(
            Config::AMQP['host'],
            Config::AMQP['login'],
            Config::AMQP['password'],
            Config::AMQP['queue']
        );
        return new MessagePreprocessorHttpApplication($validator, $sender);
    }

    public static function createWorker(): Worker
    {
        $validator = new MessageValidator();
        $provider = new AMQPClient(
            Config::AMQP['host'],
            Config::AMQP['login'],
            Config::AMQP['password'],
            Config::AMQP['queue']
        );
        $creator = new MessageCreator();
        $messageBirdClient = new Client(Config::MESSAGEBIRD_API_KEY);
        $messageBirdMessages = $messageBirdClient->messages;
        return new MessageSubmitWorker(
            $provider,
            $creator,
            $validator,
            $messageBirdMessages,
            Config::MESSAGEBIRD_TIMEOUT
        );
    }
}