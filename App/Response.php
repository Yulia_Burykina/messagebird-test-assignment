<?php

namespace App;


class Response
{
    const HTTP_SUCCESS = 200;

    const HTTP_UNPROCESSABLE_ENTITY = 422;

    const HTTP_BAD_REQUEST = 400;

    const HTTP_METHOD_NOT_ALLOWED = 405;

    const HTTP_SERVER_ERROR = 500;

    public $code;

    public $message;

    public function __construct(string $message, $code = self::HTTP_SUCCESS)
    {
        $this->message = $message;
        $this->code = $code;
    }

    public function send(): void
    {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($this->code);
        if ($this->code < 400) {
            echo json_encode(['message' => $this->message]);
        } else {
            echo json_encode(['error' => $this->message]);
        }
    }

}