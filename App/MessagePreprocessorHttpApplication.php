<?php

namespace App;


use Exception\ValidationException;
use Services\ExchangeClient;
use Services\Validator;

class MessagePreprocessorHttpApplication implements HttpApplication
{
    const RESPONSE_ONLY_POST = 'This endpoint only accepts messages by POST.';

    const RESPONSE_MALFORMED = 'Your request is malformed.';

    const RESPONSE_SUBMITTED = 'Your message was submitted for sending.';

    private $validator;

    private $sender;

    public function __construct(Validator $validator, ExchangeClient $sender)
    {
        $this->validator = $validator;
        $this->sender = $sender;
    }
    
    public function execute(array $request): Response
    {
        if ($request['method'] !== 'POST') {
            return new Response(self::RESPONSE_ONLY_POST, Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $data = json_decode($request['content'], true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return new Response(self::RESPONSE_MALFORMED, Response::HTTP_BAD_REQUEST);
        }

        try {
            $this->validator->validate($data);
            $this->sender->send(json_encode($data));
            return new Response(self::RESPONSE_SUBMITTED);
        } catch (ValidationException $e) {
            return new Response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_SERVER_ERROR);
        }
    }
}