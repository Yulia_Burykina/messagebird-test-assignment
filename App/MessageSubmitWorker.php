<?php

namespace App;

use App\Worker;
use Exception\ValidationException;
use MessageBird\Exceptions\AuthenticateException;
use MessageBird\Exceptions\BalanceException;
use MessageBird\Resources\Messages;
use Services\Creator;
use Services\ExchangeClient;
use Services\Validator;


class MessageSubmitWorker implements Worker
{
    private $provider;

    private $creator;

    private $validator;

    private $messageBirdMessages;

    private $timeout;

    public function __construct(
        ExchangeClient $provider,
        Creator $creator,
        Validator $validator,
        Messages $messageBirdMessages,
        int $timeout
    )
    {
        $this->provider = $provider;
        $this->creator = $creator;
        $this->validator = $validator;
        $this->messageBirdMessages = $messageBirdMessages;
        $this->timeout = $timeout;
    }

    public function run(): void
    {
        $this->provider->consume([$this, 'send']);
    }

    /**
     * @param string $data
     * @throws ValidationException
     * @throws AuthenticateException
     * @throws BalanceException
     * @throws \Exception
     */
    public function send(string $data): void {
        $data = json_decode($data, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Wrong message.');
        }
        $this->validator->validate($data);
        $messages = $this->creator->createFrom($data);
        foreach ($messages as $msg) {
            var_dump($this->messageBirdMessages->create($msg));
            //TODO: properly handle reload situation
            sleep($this->timeout);
        }
    }
}