<?php

namespace App;


interface HttpApplication
{
    public function execute(array $request): Response;
}