<?php

namespace App;


interface Worker
{
    public function run(): void;
}