# Test assignment for MessageBird

A simple endpoint that accepts JSON POST requests with SMSs and submits them to MessageBird API for sending.

## Prerequisites

* RabbitMQ server 3.7.*
* php-amqp extension 1.8.*
* php 7.2
* bash

## Installation

1. Clone the project code.
2. In `Config/Config.php` configure your MessageBird API key and RabbitMQ server parameters.
3. `composer install`
4. Run background worker in project directory in separate terminal: `php worker.php`
5. Run dev server in project directory in separate terminal: `php -S localhost:8000 -t ./`

## Usage
In terminal:
```
curl -d '{"body":"Some sms body.", "originator":"test", "recipients":["31624830708"]}' -H "Content-Type: application/json" -X POST http://localhost:8000
```

## Testing
In terminal (in project directory):
```
vendor/bin/phpunit
```

## Notes

This assignment is simplified because I wanted to stick to rational time limits. 
The following improvements should have been done for a proper project:

* More elaborate exception classes instead of just basic php Exceptions.
* Proper error logging for worker that handles consumption of messages from queue and their submission to MessageBird API,
  a recovery queue for messages for which the submission failed, setting up automated worker restart.
* There should be a way to find out if and when your message is actually sent. The application and worker could submit
  messages to database with different statuses ('accepted', 'submitted', 'sent', 'failed'), 
  and there could be additional worker that would ask MessageBird API for actual sending status to update it. This way 
  the response could redirect you to status page.
* Handle long unicode messages properly. Now I am out of balance to make futher assumption why they failed.
* Dockerize the project.