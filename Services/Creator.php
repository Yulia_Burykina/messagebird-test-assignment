<?php

namespace Services;


interface Creator
{
    public function createFrom(array $data): array;
}