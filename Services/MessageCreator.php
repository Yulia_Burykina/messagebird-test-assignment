<?php

namespace Services;


use MessageBird\Objects\Message;

class MessageCreator implements Creator
{
    /**
     * TODO: properly handle unicode data
     *
     * @param array $data
     * @return array
     */
    public function createFrom(array $data): array
    {
        $messages = [];

        //we're counting bytes
        if (\strlen($data['body']) > 160) {
            //splitting bytes, too
            $parts = str_split($data['body'], 153);
            $ref = rand(1, 255);
            $udhPart = '050003' . $this->toHex($ref) . $this->toHex(\count($parts));
            $i = 1;
            foreach ($parts as $part) {
                $msg = new Message();
                $msg->originator = $data['originator'];
                $msg->recipients = $data['recipients'];
                $msg->setBinarySms(
                    $udhPart . $this->toHex($i),
                    unpack('H*', $part)[1]
                );
                $messages[]= $msg;
                $i++;
            }
        } else {
            $msg = new Message();
            $msg->loadFromArray($data);
            $messages[] = $msg;
        }
        return $messages;
    }

    private function toHex(int $num): string
    {
        return ($num <= 15) ? '0' . dechex($num) : dechex($num);
    }
}