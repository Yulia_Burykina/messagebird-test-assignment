<?php

namespace Services;


use Exception\ValidationException;

class MessageValidator implements Validator
{
    const WRONG_FIELD_STRUCTURE = 'Message should have exactly 3 fields: originator, recipients and body.';

    const WRONG_RECIPIENT_FIELD_STRUCTURE = 'Recipients field should be non-empty array.';

    const WRONG_RECIPIENT = 'All recipients should be numbers no longer than 15 digits.';

    const WRONR_ORIGINATOR = 'Originator should be number no longer than 15 digits or alphanumeric string with no less than 3 and no more than 11 characters.';

    const BODY_TOO_LONG = "Message body shouldn't be longer than 1377 characters.";

    const TOO_MANY_RECIPIENTS = "There shouldn't be more than 50 recipients.";

    const EMPTY_BODY = "Message body shouldn't be empty.";

    const WRONG_BODY = 'Message body should be valid string';

    const MAX_BODY_LENGTH = 1377;

    const MAX_RECIPIENTS = 50;

    /**
     * @param array $data
     * @throws ValidationException
     */
    public function validate(array $data): void
    {
        if (\count($data) !== 3 || !isset($data['originator'], $data['recipients'], $data['body'])) {
            throw new ValidationException(self::WRONG_FIELD_STRUCTURE);
        }

        if (!\is_array($data['recipients']) || empty($data['recipients'])) {
            throw new ValidationException(self::WRONG_RECIPIENT_FIELD_STRUCTURE);
        }

        if (\count($data['recipients']) > self::MAX_RECIPIENTS) {
            throw new ValidationException(self::TOO_MANY_RECIPIENTS);
        }

        if (!\is_string($data['body'])) {
            throw new ValidationException(self::WRONG_BODY);
        }

        if (empty($data['body'])) {
            throw new ValidationException(self::EMPTY_BODY);
        }

        //we use strlen because we're counting bytes, not chars
        if (\strlen($data['body']) > self::MAX_BODY_LENGTH) {
            throw new ValidationException(self::BODY_TOO_LONG);
        }

        if (!preg_match('/^\w{3,11}$/', $data['originator']) &&
            !preg_match('/^[1-9]\d{1,15}$/', $data['originator'])) {
            throw new ValidationException(self::WRONR_ORIGINATOR);
        }

        foreach ($data['recipients'] as $rec) {
            if (!preg_match('/^[1-9]\d{1,15}$/', $rec)) {
                throw new ValidationException(self::WRONG_RECIPIENT);
            }
        }
    }
}