<?php

namespace Services;


interface ExchangeClient
{
    public function send(string $message): void;

    public function consume(callable $callback): void;
}