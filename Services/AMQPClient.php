<?php

namespace Services;


class AMQPClient implements ExchangeClient
{
    private $host;
    private $login;
    private $password;
    private $queueTitle;

    /** @var \AMQPQueue */
    private $queue;

    /** @var \AMQPExchange */
    private $exchange;

    public function __construct(
        string $host,
        string $login,
        string $password,
        string $queueTitle
    )
    {
        $this->host = $host;
        $this->login = $login;
        $this->password = $password;
        $this->queueTitle = $queueTitle;
    }

    /**
     * @param string $message
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     * @throws \AMQPQueueException
     */
    public function send(string $message): void
    {
        $this->connect();
        $this->exchange->publish($message);
    }

    /**
     * @param callable $callback
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPEnvelopeException
     * @throws \AMQPExchangeException
     * @throws \AMQPQueueException
     */
    public function consume(callable $callback): void
    {
        $this->connect();
        $this->queue->consume(function (\AMQPEnvelope $envelope) use (&$callback) {
            $body = $envelope->getBody();
            try {
                $callback($body);
            } catch (\Exception $e) {
                $this->queue->reject($envelope->getDeliveryTag());
                //TODO: proper error logging
                echo 'Error while handling message "' . $envelope->getBody() . '": ' . $e->getMessage();
                return;
            }
            $this->queue->ack($envelope->getDeliveryTag());
        });
    }
    /**
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     * @throws \AMQPChannelException
     * @throws \AMQPQueueException
     */
    private function connect(): void
    {
        if ($this->exchange !== null && $this->queue !== null) {
            return;
        }
        $connection = new \AMQPConnection([
            'host' => $this->host,
            'login' => $this->login,
            'password' => $this->password
        ]);
        //persistent reusable connection
        $connection->pconnect();

        $channel = new \AMQPChannel($connection);

        $this->exchange = new \AMQPExchange($channel);
        $this->exchange->setName($this->queueTitle);
        $this->exchange->setType(AMQP_EX_TYPE_DIRECT);
        $this->exchange->setFlags(AMQP_DURABLE);
        $this->exchange->declareExchange();

        $this->queue = new \AMQPQueue($channel);
        $this->queue->setName($this->queueTitle);
        $this->queue->setFlags(AMQP_DURABLE);
        $this->queue->declareQueue();
        $this->queue->bind($this->queueTitle);
    }


}