<?php

namespace Services;


use Exception\ValidationException;

interface Validator
{
    /**
     * @param array $data
     * @throws ValidationException
     */
    public function validate(array $data): void;
}