<?php

namespace Tests\Unit;

use App\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{

    /**
     * @runInSeparateProcess
     */
    public function testSendSuccess()
    {
        $response = new Response('test');
        ob_start();
        $response->send();
        $output = ob_get_clean();
        $this->assertJsonStringEqualsJsonString('{"message":"test"}', $output);
        
    }

    /**
     * @runInSeparateProcess
     */
    public function testSendError()
    {
        $response = new Response('test', Response::HTTP_BAD_REQUEST);
        ob_start();
        $response->send();
        $output = ob_get_clean();
        $this->assertJsonStringEqualsJsonString('{"error":"test"}', $output);
    }
}
