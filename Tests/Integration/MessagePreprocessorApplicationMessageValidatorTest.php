<?php

namespace Tests\Integration;

use PHPUnit\Framework\TestCase;
use App\MessagePreprocessorHttpApplication;
use App\Response;
use Services\ExchangeClient;
use Services\MessageValidator;

class MessagePreprocessorApplicationMessageValidatorTest extends TestCase
{
    private $sentMessage;

    private $app;

    public function setUp() {
        $validator = new MessageValidator();
        $senderMock = $this->createMock(ExchangeClient::class);

        $senderMock->method('send')
            ->will($this->returnCallback([$this, 'sendMock']));
        $this->app = new MessagePreprocessorHttpApplication($validator, $senderMock);
    }


    private function generateRandomString($length, $onlyDigits = false)
    {
        if (!$onlyDigits) {
            $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } else {
            $x = '123456789';
        }
        return substr(str_shuffle(str_repeat(
            $x,
            ceil($length/strlen($x)) )),
            1,
            $length
        );
    }

    private function generateRecipients($length)
    {
        $result = [];
        for ($i = 0; $i < $length; $i++) {
            $result[] = '"' . $this->generateRandomString(12, true) . '"';
        }
        return implode(',', $result);
    }

    public function sendMock($msg) {
        $this->sentMessage = $msg;
    }

    /**
     * @dataProvider datasets
     */
    public function testExecuteErrors($method, $content, $responseMessage, $responseCode)
    {
        $response = $this->app->execute([
            'method' => $method,
            'content' => $content
        ]);
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals($responseMessage, $response->message);
        $this->assertEquals($responseCode, $response->code);
    }

    public function testExecute()
    {
        $response = $this->app->execute([
            'method' => 'POST',
            'content' => '{"body":"test body", "originator":"test", "recipients":["123456789"]}'
        ]);
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(MessagePreprocessorHttpApplication::RESPONSE_SUBMITTED, $response->message);
        $this->assertEquals(Response::HTTP_SUCCESS, $response->code);
        $this->assertJsonStringEqualsJsonString(
            '{"body":"test body", "originator":"test", "recipients":["123456789"]}',
            $this->sentMessage
            );
    }

    public function datasets()
    {
        return [
            [
                'GET',
                '{"body":"test body", "originator":"test", "recipients":["123456789"]}',
                MessagePreprocessorHttpApplication::RESPONSE_ONLY_POST,
                Response::HTTP_METHOD_NOT_ALLOWED
            ],
            [
                'POST',
                '',
                MessagePreprocessorHttpApplication::RESPONSE_MALFORMED,
                Response::HTTP_BAD_REQUEST
            ],
            [
                'POST',
                '{"originator":"test", "recipients":["123456789"]}',
                MessageValidator::WRONG_FIELD_STRUCTURE,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":"test body", "originator":"test", "recipients":[]}',
                MessageValidator::WRONG_RECIPIENT_FIELD_STRUCTURE,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":["test body"], "originator":"test", "recipients":["123456789"]}',
                MessageValidator::WRONG_BODY,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":"", "originator":"test", "recipients":["123456789"]}',
                MessageValidator::EMPTY_BODY,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":"test body", "originator":"test#%", "recipients":["123456789"]}',
                MessageValidator::WRONR_ORIGINATOR,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":"test body", "originator":"test", "recipients":["12345678910111213"]}',
                MessageValidator::WRONG_RECIPIENT,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":"test body", "originator":"test", "recipients":[' .
                    $this->generateRecipients(MessageValidator::MAX_RECIPIENTS +1) .
                    ']}',
                MessageValidator::TOO_MANY_RECIPIENTS,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'POST',
                '{"body":"' .
                    $this->generateRandomString(MessageValidator::MAX_BODY_LENGTH + 1) .
                    '", "originator":"test", "recipients":["123456789"]}',
                MessageValidator::BODY_TOO_LONG,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ]
        ];
    }
}