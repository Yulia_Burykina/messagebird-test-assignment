<?php
/**
 * Created by PhpStorm.
 * User: avedev
 * Date: 19.10.18
 * Time: 19:28
 */

namespace Tests\Integration;

use MessageBird\Objects\Message;
use MessageBird\Resources\Messages;
use PHPUnit\Framework\TestCase;
use Services\ExchangeClient;
use Services\MessageCreator;
use App\MessageSubmitWorker;
use Services\MessageValidator;

class MessageSubmitterMessageCreatorTest extends TestCase
{
    private $sentMessages = [];

    private $submitter;

    public function setUp() {
        //because we already tested validator in app integration test
        $validator = $this->createMock(MessageValidator::class);
        $messageBird = $this->createMock(Messages::class);
        $messageBird->method('create')
            ->will($this->returnCallback([$this, 'sendMock']));
        $provider = $this->createMock(ExchangeClient::class);

        $this->submitter = new MessageSubmitWorker(
            $provider,
            new MessageCreator(),
            $validator,
            $messageBird,
            0
        );
    }

    public function sendMock($msg) {
        $this->sentMessages[] = $msg;
    }

    public function testSendOneMessage()
    {
        $message = '{"body":"test body", "originator":"test", "recipients":["123456789"]}';
        $this->submitter->send($message);
        $this->assertCount(1, $this->sentMessages);
        $this->assertInstanceOf(Message::class, $this->sentMessages[0]);
        $this->assertEquals('test body', $this->sentMessages[0]->body);
        $this->assertEquals('test', $this->sentMessages[0]->originator);
        $this->assertEquals(['123456789'], $this->sentMessages[0]->recipients);
    }

    /**
     * @dataProvider datasets
     *
     * @param string $body
     */
    public function testSeveralMessages($body)
    {
        $message = '{"body":"'.$body.'", "originator":"test", "recipients":["123456789"]}';
        $this->submitter->send($message);
        $this->assertCount(2, $this->sentMessages);
        $hexParts = [];
        $ref = substr($this->sentMessages[0]->typeDetails['udh'], 6, 2);
        foreach ($this->sentMessages as $key => $msg) {
            $this->assertInstanceOf(Message::class, $msg);
            $this->assertEquals('test', $msg->originator);
            $this->assertEquals(['123456789'], $msg->recipients);
            $this->assertEquals(Message::TYPE_BINARY, $msg->type);
            $this->assertEquals('050003' . $ref . '020' . ($key+1), $msg->typeDetails['udh']);
            $hexParts[] = $msg->body;
        }
        $this->assertEquals(153*2, \strlen($this->sentMessages[0]->body));
        $this->assertEquals($body, pack('H*', implode('', $hexParts)));
    }

    public function datasets()
    {
        return [
            ['We differentiate between the length of a single message and the length'.
                ' of a single SMS. A standard SMS can contain up to 160 characters, '.
                'depending on the alphabet used in your SMS.'],
            ['Какое-то русское сообщение с кучей буков в юникоде омг, что теперь '.
                'делать, без паники, с ним эта фича тоже сработает! (я котик, у меня лапки)'
            ]
        ];
    }

    public function testInvalidMessage()
    {
        try {
            $this->submitter->send('');
        } catch (\Exception $e) {
            $this->assertEquals('Wrong message.', $e->getMessage());
            return;
        }
        $this->fail();
    }

}