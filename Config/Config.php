<?php

namespace Config;


class Config
{
    const MESSAGEBIRD_API_KEY = 'your_api_key';

    const AMQP = [
        'host' => 'localhost',
        'login' => 'guest',
        'password' => 'guest',
        'queue' => 'sms'
    ];

    const MESSAGEBIRD_TIMEOUT = 1;
}